/* eslint-disable linebreak-style */
/* eslint-disable indent */
export default [
    {
        title: 'Радионяня Motorola MBP16',
        price: 3690,
        image: 'img/radio.jpg',
    },
    {
        title: 'Ультразвуковая зубная щётка Playbrush Smart Sonic',
        price: 5660,
        image: 'img/toothbrush.jpg',
    },
    {
        title: 'Смартфон Xiaomi Mi Mix 3 6/128GB',
        price: 21790,
        image: 'img/phone.jpg',
    },
    {
        title: 'Электроскейт Razor Cruiser',
        price: 24690,
        image: 'img/board.jpg',
    },
    {
        title: 'Смартфон Xiaomi Mi A3 4/64GB Android One',
        price: 14960,
        image: 'img/phone-2.jpg',
    },
    {
        title: 'Смартфон Xiaomi Redmi 6/128GB',
        price: 8960,
        image: 'img/phone-3.jpg',
    },
    {
        title: 'Электрический дрифт-карт Razor Crazy Cart',
        price: 39900,
        image: 'img/bicycle.jpg',
    },
    {
        title: 'Гироскутер Razor Hovertrax 2.0',
        price: 34900,
        image: 'img/wheels.jpg',
    },
    {
        title: 'Детский трюковой самокат Razor Grom',
        price: 4990,
        image: 'img/scooter.jpg',
    },
    {
        title: 'Роллерсёрф Razor RipStik Air Pro',
        price: 6690,
        image: 'img/ripstik.jpg',
    },
    {
        title: 'Наушники AirPods с беспроводным зарядным футляром',
        price: 16560,
        image: 'img/airpods.jpg',
    },
    {
        title: 'Наушники Sony',
        price: 30690,
        image: 'img/headphones.jpg',
    },
    {
        title: 'Наушники Razer Hammerhead True Wireless Pro',
        price: 12989,
        image: 'img/razer-hammerhead-true-wireless-pro-black-product-promo.png',
    },
    {
        title: 'Игровая мышь Razer Mamba Elite',
        price: 30690,
        image: 'img/razer-mamba-elite.png',
    },
    {
        title: 'Клавиатура Razer BlackWidow V3 Mini HyperSpeed (Green Switch)',
        price: 12999,
        image: 'img/razer-blackwidow-v3-mini-hyperspeed-product-promo.png',
    },
    {
        title: 'Коврик для мыши Razer Firefly V2',
        price: 4399,
        image: 'img/razer-firefly-v2-product-promo.png',
    },
    {
        title: 'Геймпад Razer Wolverine V2 Chroma',
        price: 11999,
        image: 'img/razer-wolverine-v2-chroma-product-promo.png',
    },
    {
        title: 'Игровое кресло Razer Iskur - Black / Green - XL',
        price: 50599,
        image: 'img/razer-iskur-xl-product-promo.png',
    },
    {
        title: 'Корпус для игрового ПК Razer Tomahawk Mini-ITX',
        price: 15499,
        image: 'img/razer-tomahawk-mini-atx-product-promo.png',
    },
    {
        title: 'Рюкзак Razer Rogue Backpack 17.3” V3 - Chromatic Edition',
        price: 12199,
        image: 'img/razer-rogue-backpack-17-v3-chromatic-product-promo.png',
    },
    {
        title: 'Держатель провода мыши Razer Mouse Bungee V3 Chroma',
        price: 3499,
        image: 'img/razer-mouse-bungee-v3-chroma.png',
    },
    {
        title: 'Razer Arctech Pro for iPhone 12 Pro Max (6.7”)',
        price: 1799,
        image: 'img/razer-artech-pro-for-iphone-12-pro-max.png',
    },
    {
        title: 'Аудиоколонки Razer Nommo Chroma',
        price: 12999,
        image: 'img/nommo-chroma-promo.png',
    },
    {
        title: 'Веб-камера Razer Kiyo X',
        price: 5599,
        image: 'img/razer-kiyo-x-product-promo.png',
    },
];
